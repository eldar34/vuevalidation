import VueRouter from 'vue-router'
import Home from './pages/Home'
import ErrorCmp from './pages/Error'
import CarFull from './pages/CarFull'

const Cars = resolve => {
    require.ensure(['./pages/Cars.vue'], () => {
        resolve(
            require('./pages/Cars.vue')
        )
    })
}

const Car = resolve => {
    require.ensure(['./pages/Car.vue'], () => {
        resolve(
            require('./pages/Car')
        )
    })
}

export default new VueRouter({
    routes: [
        {
            path: '',
            component: Home
        },
        {
            path: '/cars',
            component: Cars
        },
        {
            path: '/car/:id',
            component: Car,
            children: [
                {
                    path: 'full', // localhost:8080/car/2/full
                    component: CarFull,
                    name: 'carFull'
                }
            ]
        },
        {
            path: '/none',
            //redirect: '/cars'
            redirect: {
                name: 'carFull'
            }
        },
        {
            path: '*',
            component: ErrorCmp
        }
    ],
    mode: 'history',
    scrollBehavior(to, savedPosition) {

        if(savedPosition){
            return savedPosition
        }

        if(to.hash){
            return {
                selector: to.hash
            }
        }

        return {
            x: 0,
            y: 500
        }
    }
})